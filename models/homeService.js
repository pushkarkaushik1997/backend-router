var mongoose=require('mongoose');
var home=mongoose.Schema({
    selectCategory:String,
    businessName:String,
    address:String,
    email:String,
    phoneNo:Number,
    image:String,
    city:String,
    state:String,
    zip:Number,
    quickInformation:String
    
    
})
module.exports=mongoose.model('Home',home)