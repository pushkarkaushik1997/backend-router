var mongoose=require('mongoose');
var training=mongoose.Schema({
    selectCategory:String,
    businessName:String,
    address:String,
    email:String,
    phoneNo:Number,
    image:String,
    city:String,
    state:String,
    zip:Number,
    quickInformation:String
    
    
})
module.exports=mongoose.model('Training',training)