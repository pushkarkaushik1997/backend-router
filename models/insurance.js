var mongoose=require('mongoose');
var insurance=mongoose.Schema({
    selectCategory:String,
    businessName:String,
    address:String,
    email:String,
    phoneNo:Number,
    image:String,
    city:String,
    state:String,
    zip:Number,
    quickInformation:String
    
    
})
module.exports=mongoose.model('Insurance',insurance)