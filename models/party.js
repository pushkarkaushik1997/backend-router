var mongoose=require('mongoose');

var party = mongoose.Schema({
    selectCategory:String,
    businessName:String,
    emailId:String,
    ContactNumber:Number,
    address:String,
    city:String,
    state:String,
    zip:Number,
    timing:String,
    bannerImage:String,
    attachment:String,
    productAndServicesOffered:String
    
    

})

module.exports= mongoose.model('party',party)