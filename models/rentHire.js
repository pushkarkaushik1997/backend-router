var mongoose=require('mongoose');
var rent=mongoose.Schema({
    selectCategory:String,
    businessName:String,
    address:String,
    email:String,
    phoneNo:Number,
    image:String,
    city:String,
    state:String,
    zip:Number,
    quickInformation:String
    
    
})
module.exports=mongoose.model('Rent',rent)