var mongoose = require('mongoose')


var doctors = mongoose.Schema({
    selectCategory:String,
    businessName:String,
    image:String,
    phoneNo:Number,
    quickInformation:String,
    address:String,
    city:String,
    state:String,
    zip:Number
})

module.exports = mongoose.model('Doctors',doctors)