var mongoose = require('mongoose')

var photography = mongoose.Schema({
    selectCategory:String,
    businessName:String,
    emailId:String,
    ContactNumber:Number,
    address:String,
    city:String,
    state:String,
    zip:Number,
    HoursOfOperation:String,
    bannerImage:String,
    attachment:String,
    productAndServicesOffered:String

})

module.exports = mongoose.model('photography',photography);