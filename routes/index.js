
var express = require('express');
var router = express.Router();
var Restaurants = require('../models/restaurants');
var party = require('../models/party');
var acServices = require('../models/acService');
var caterers = require('../models/caterers');
var photography = require('../models/photography');
var wedding = require('../models/wedding');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

           /* Add Restaurants*/
router.post('/restaurants',(req,res)=>{
  new Restaurants({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    image:req.body.image,
    phoneNo:req.body.phoneNo,
    quickInformation:req.body.quickInformation,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip
  }).save((err,data)=>{
    if (err) {
      console.log(err)
    } else {
      console.log('Add restaurants is created');
      res.json(data);
    }
  })
})

  // read data of Restaurants
router.post('/read',(req,res)=>{
  Restaurants.find((err,data)=>{
    if (err) {
      console.log(err)
    } else {
      res.json(data)
    }
  })
})

// update data in restaurants 



router.post('/party',(req,res)=>{
  new party({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    emailId:req.body.emailId,
    ContactNumber:req.body.ContactNumber,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    timing:req.body.timing,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.productAndServicesOffered
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
})
})

router.post('/partysearch',(req,res)=>{
  party.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/partyupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  party.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/partydelete',(req,res)=>{
  party.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})
router.post('/acServices',(req,res)=>{
  new acServices({
    selectCategory:req.body.select1,
    businessName:req.body.name1,
    emailId:req.body.email1,
    ContactNumber:req.body.contact1,
    address:req.body.address1,
    city:req.body.city1,
    state:req.body.state1,
    zip:req.body.zip1,
    timing:req.body.timing1,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.detail1
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      //res.json(data)
      res.json({message:"YOu want to continue"})
    }
})
})

router.post('/acServicesearch',(req,res)=>{
  acServices.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/acServicesupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  acServices.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/acServicesdelete',(req,res)=>{
  acServices.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})


router.post('/caterers',(req,res)=>{
  new caterers({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    emailId:req.body.emailId,
    ContactNumber:req.body.ContactNumber,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    HoursOfOperation:req.body.HoursOfOperation,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.productAndServicesOffered
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
})
})

router.post('/catererssearch',(req,res)=>{
  caterers.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/caterersupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  caterers.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/caterersdelete',(req,res)=>{
  caterers.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})

router.post('/wedding',(req,res)=>{
  new wedding({
    selectCategory:req.body.selectCategory,
    businessName:req.body.wname,
    emailId:req.body.wemail,
    ContactNumber:req.body.ContactNumber,
    address:req.body.waddress,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    HoursOfOperation:req.body.HoursOfOperation,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.productAndServicesOffered
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      //res.json(data)
      res.json({message:"Continue"})
    }
})
})

router.post('/weddingsearch',(req,res)=>{
  wedding.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/weddingupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  wedding.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/weddingdelete',(req,res)=>{
  wedding.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})

router.post('/photography',(req,res)=>{
  new photography({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    emailId:req.body.emailId,
    ContactNumber:req.body.ContactNumber,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    HoursOfOperation:req.body.HoursOfOperation,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.productAndServicesOffered
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
})
})

router.post('/photographysearch',(req,res)=>{
  photography.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/photographyupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  photography.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/photographydelete',(req,res)=>{
  photography.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})


module.exports = router;

var express = require('express');
var router = express.Router();
var Restaurants = require('../models/restaurants');

var Education = require('../models/education');
var Home=require('../models/homeService');
var Insurance=require('../models/insurance');
var Rent=require('../models/rentHire');
var Training=require('../models/training');

var Travel = require('../models/travel');
var Doctors = require('../models/doctors');
var party = require('../models/party');
var acServices = require('../models/acService');
var caterers = require('../models/caterers');
var photography = require('../models/photography');
var wedding = require('../models/wedding');


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Express'
  });
});

/* Add Restaurants*/
router.post('/restaurants', (req, res) => {
  new Restaurants({
    selectCategory: req.body.selectCategory,
    businessName: req.body.businessName,
    image: req.body.image,
    phoneNo: req.body.phoneNo,
    quickInformation: req.body.quickInformation,
    address: req.body.address,
    city: req.body.city,
    state: req.body.state,
    zip: req.body.zip
  }).save((err, data) => {
    if (err) {
      console.log(err)
    } else {
      console.log('Add restaurants is created');
      res.json(data);
    }
  })
})

// read data of Restaurants
router.post('/read', (req, res) => {
  Restaurants.find((err, data) => {
    if (err) {
      console.log(err)
    } else {
      res.json(data)
    }
  })
})
//////////////////////////////////////Education
/* Add Education*/
router.post('/addedu', (req, res) => {
  new Education({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    address:req.body.address,
    email:req.body.email,
    phoneNo:req.body.phoneNo,
    image:req.body.image,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    quickInformation:req.body.quickInformation
  }).save((err, data) => {
    if (err) {
      console.log(err)
    } else {
      console.log('Add education is created');
      res.json(data);
    }
  })
})

// read data of education
router.post('/readedu', (req, res) => {
  Education.find((err, data) => {
    if (err) {
      console.log(err)
    } else {
      res.json(data)
    }
  })
})

///update data of education
router.post('/updateedu',(req,res)=>{

  let obj={
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    address:req.body.address,
    email:req.body.email,
    phoneNo:req.body.phoneNo,
    image:req.body.image,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    quickInformation:req.body.quickInformation
  }
  Education.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err)
      console.log(err);
    else
      res.json({message:"Item has been updated"});
  })
})

///delete data of update
router.post('/deleteEdu',(req,res)=>{
  Education.findByIdAndRemove(req.body._id,(err,data)=>{
    if (err) {
      console.log(err)
    } else 
      res.json({message:"Item has been deleted"});
    
  })
})

///////////////Home-Services
/* Add Home*/
router.post('/addhome', (req, res) => {
  new Home({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    address:req.body.address,
    email:req.body.email,
    phoneNo:req.body.phoneNo,
    image:req.body.image,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    quickInformation:req.body.quickInformation
  }).save((err, data) => {
    if (err) {
      console.log(err)
    } else {
      console.log('Add education is created');
      res.json(data);
    }
  })
})

// read data of home
router.post('/readhome', (req, res) => {
  Home.find((err, data) => {
    if (err) {
      console.log(err)
    } else {
      res.json(data)
    }
  })
})

///update data of home
router.post('/updatehome',(req,res)=>{

  let obj={
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    address:req.body.address,
    email:req.body.email,
    phoneNo:req.body.phoneNo,
    image:req.body.image,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    quickInformation:req.body.quickInformation
  }
  Home.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err)
      console.log(err);
    else
      res.json({message:"Item has been updated"});
  })
})

///delete data of home
router.post('/deletehome',(req,res)=>{
  Home.findByIdAndRemove(req.body._id,(err,data)=>{
    if (err) {
      console.log(err)
    } else 
      res.json({message:"Item has been deleted"});
    
  })
})

/////////////////////Insurance

/* Add Insurance*/
router.post('/addinsurance', (req, res) => {
  new Insurance({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    address:req.body.address,
    email:req.body.email,
    phoneNo:req.body.phoneNo,
    image:req.body.image,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    quickInformation:req.body.quickInformation
  }).save((err, data) => {
    if (err) {
      console.log(err)
    } else {
      console.log('Add education is created');
      res.json(data);
    }
  })
})

// read data of Insurance
router.post('/readInsurance', (req, res) => {
  Insurance.find((err, data) => {
    if (err) {
      console.log(err)
    } else {
      res.json(data)
    }
  })
})

///update data of Insurance
router.post('/updateInsurance',(req,res)=>{

  let obj={
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    address:req.body.address,
    email:req.body.email,
    phoneNo:req.body.phoneNo,
    image:req.body.image,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    quickInformation:req.body.quickInformation
  }
  Insurance.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err)
      console.log(err);
    else
      res.json({message:"Item has been updated"});
  })
})

///delete data of Insurance
router.post('/deleteInsurance',(req,res)=>{
  Insurance.findByIdAndRemove(req.body._id,(err,data)=>{
    if (err) {
      console.log(err)
    } else 
      res.json({message:"Item has been deleted"});
    
  })})
// update data in restaurants 
 router.post('/update',(req,res)=>{
   let obj={selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    image:req.body.image,
    phoneNo:req.body.phoneNo,
    quickInformation:req.body.quickInformation,
    address:req.body.address,
    
    zip:req.body.zip}
    Restaurants.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
      if (err) {
        console.log(err)
      } else {
        res.json({message: "Update sucessfull"})
      }
    })
 })

//  delete data in restaurants
router.post('/delete',(req,res)=>{
  Restaurants.findByIdAndRemove(req.body._id,(err,data)=>{
    if (err) {
      console.log(err)
    } else {
      res.json({Message:"item has been deleted"})
    }
  })
})

// //////////////////////////////////////////// Travel Travel Travel Travel Travel /////////////////////////////////////////////////////////////

           /* Add Travel*/
router.post('/addTravel',(req,res)=>{
  new Travel({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    image:req.body.image,
    phoneNo:req.body.phoneNo,
    quickInformation:req.body.quickInformation,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip
  }).save((err,data)=>{
    if (err) {
      console.log(err)
    } else {
      console.log('Add travel is created');
      res.json(data);
    }
  })
})

 // read data of Travel
 router.post('/readTravel',(req,res)=>{
  Travel.find((err,data)=>{
    if (err) {
      console.log(err)
    } else {
      res.json(data)
    }
  })
})


// update data in Travel 
router.post('/updateTravel',(req,res)=>{
  let obj={selectCategory:req.body.selectCategory,
   businessName:req.body.businessName,
   image:req.body.image,
   phoneNo:req.body.phoneNo,
   quickInformation:req.body.quickInformation,
   address:req.body.address,
   
   zip:req.body.zip}
   Travel.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
     if (err) {
       console.log(err)
     } else {
       res.json({message: "Update sucessfull"})
     }
   })
})

//  delete data in restaurants
router.post('/deleteTravel',(req,res)=>{
  Travel.findByIdAndRemove(req.body._id,(err,data)=>{
    if (err) {
      console.log(err)
    } else {
      res.json({Message:"item has been deleted"})
    }
  })
})

/////////////////////////////////////////////////////// Doctors ///////////////////////////////////

//////// create data in doctors /////////////////////

router.post('/addDoctors',(req,res)=>{
  new Doctors({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    image:req.body.image,
    phoneNo:req.body.phoneNo,
    quickInformation:req.body.quickInformation,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip
  }).save((err,data)=>{
    if (err) {
      console.log(err)
    } else {
      res.json(data)
    }
  })
})


//////////////////////// find data in doctors ///////////////////

router.post('/findDoctors',(req,res)=>{
  Doctors.find((err,data)=>{
    if (err) {
      console.log(err)
    } else {
      res.json(data)
    }
  })
})

router.post('/party',(req,res)=>{
  new party({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    emailId:req.body.emailId,
    ContactNumber:req.body.ContactNumber,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    timing:req.body.timing,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.productAndServicesOffered
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
})
})

router.post('/partysearch',(req,res)=>{
  party.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/partyupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  party.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/partydelete',(req,res)=>{
  party.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})
router.post('/acServices',(req,res)=>{
  new acServices({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    emailId:req.body.emailId,
    ContactNumber:req.body.ContactNumber,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    timing:req.body.timing,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.productAndServicesOffered
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
})
})

router.post('/acServicesearch',(req,res)=>{
  acServices.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/acServicesupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  acServices.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/acServicesdelete',(req,res)=>{
  acServices.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})


router.post('/caterers',(req,res)=>{
  new caterers({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    emailId:req.body.emailId,
    ContactNumber:req.body.ContactNumber,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    HoursOfOperation:req.body.HoursOfOperation,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.productAndServicesOffered
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
})
})

router.post('/catererssearch',(req,res)=>{
  caterers.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/caterersupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  caterers.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/caterersdelete',(req,res)=>{
  caterers.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})

router.post('/wedding',(req,res)=>{
  new wedding({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    emailId:req.body.emailId,
    ContactNumber:req.body.ContactNumber,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    HoursOfOperation:req.body.HoursOfOperation,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.productAndServicesOffered
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
})
})

router.post('/weddingsearch',(req,res)=>{
  wedding.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/weddingupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  wedding.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/weddingdelete',(req,res)=>{
  wedding.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})

router.post('/photography',(req,res)=>{
  new photography({
    selectCategory:req.body.selectCategory,
    businessName:req.body.businessName,
    emailId:req.body.emailId,
    ContactNumber:req.body.ContactNumber,
    address:req.body.address,
    city:req.body.city,
    state:req.body.state,
    zip:req.body.zip,
    HoursOfOperation:req.body.HoursOfOperation,
    bannerImage:req.body.bannerImage,
    attachment:req.body.attachment,
    productAndServicesOffered:req.body.productAndServicesOffered
  }).save((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
})
})

router.post('/photographysearch',(req,res)=>{
  photography.find((err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json(data)
    }
  })
})

router.post('/photographyupdate',(req,res)=>{
  let obj ={
    selectCategory:req.body.selectCategory
  }
  photography.findByIdAndUpdate(req.body._id,obj,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"updated"})
    }
  })
})

router.post('/photographydelete',(req,res)=>{
  photography.findByIdAndDelete(req.body._id,(err,data)=>{
    if(err){
      console.log(err)
    }
    else{
      res.json({message:"deleted"})
    }
  })

})


module.exports = router;

